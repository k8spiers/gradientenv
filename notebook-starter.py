#!/usr/bin/env python
import logging
import os
import subprocess
import sys
import time
import json
from argparse import ArgumentParser
from pathlib import Path

import yaml
from gradient import NotebooksClient
from gradient.api_sdk.sdk_exceptions import ResourceFetchingError
from jinja2 import Environment, FileSystemLoader

# Some config variables
DEFAULT_NOTEBOOK_OPTIONS_FILE = "options.yaml.j2"
DEFAULT_MAX_NOTEBOOKS = 10
DEFAULT_EXISTING_NOTEBOOK_POLICY = "abort"
EXISTING_NOTEBOOK_WAIT_INTERVAL = 10
DEFAULT_LIVENESS_INTERVAL = 10
DEFAULT_LIVENESS_ATTEMPTS = 50
DEFAULT_SSH_USERNAME_ENVIRONMENT_VARIABLE = "TAILSCALE_USER"
FREE_MACHINE_TYPES = [
    "Free-A4000",
    "Free-P5000",
    "Free-RTX5000",
    "Free-RTX4000",
    "Free-GPU",
]


def load_options(filepath):
    """
    Load Gradient notebook YAML+jinja2 file.
    """

    # Create a Jinja2 environment
    env = Environment(loader=FileSystemLoader("./"))

    # Define an "env_override" macro to read environment variables named in the file
    env.filters["env_override"] = lambda default, key: os.getenv(key, default)

    # Create and render the template
    template = env.get_template(str(filepath))
    rendered = template.render(getenv=lambda key: os.environ[key])

    # Load as YAML and return
    options = yaml.safe_load(rendered)
    assert isinstance(options, dict)
    return options


def tailscale_status(timeout=None):
    """
    Fetch tailscale status in JSON format using the tailscale shell command
    Return unmarshalled JSON (should be a dictionary)
    """
    # WARNING: "tailscale status -h" indicates that format of "tailscale status --json"
    #          output is subject to change
    subprocess_command = ["tailscale", "status", "--json"]
    logging.debug(" ".join(subprocess_command))
    try:
        completed = subprocess.run(
            subprocess_command,
            check=True,
            timeout=timeout,
            capture_output=True,
        )
    except subprocess.CalledProcessError as err:
        logging.debug("err.cmd: %r", err.cmd)
        logging.debug("err.output: %r", err.output)
        logging.debug("err.stdout: %r", err.stdout)
        logging.debug("err.stderr: %r", err.stderr)
        raise
    except subprocess.TimeoutExpired as err:
        logging.debug("timeout")
        raise
    return json.loads(completed.stdout)


def wait_until_live(
        hostname,
        poll_interval_seconds=DEFAULT_LIVENESS_INTERVAL,
        attempts=DEFAULT_LIVENESS_ATTEMPTS):
    """
    Poll tailscale status until hostname is live
    Return True when hostname is live, else False upon timeout or error
    """
    for i in range(attempts):
        logging.debug(
            "Waiting for %r to appear among tailscale peers, attempt %d/%d",
            hostname, i + 1, attempts)
        status = tailscale_status(30)
        hostnames = [peer["HostName"] for peer in status["Peer"].values()]
        logging.debug("Current tailscale peer hostnames: %r", hostnames)
        if hostname in hostnames:
            return True
        time.sleep(poll_interval_seconds)
    return False


def test_ssh_connection(hostname, username, command="ls"):
    """
    Attempt SSH connection and return True if successful, else False.
    """
    subprocess_command = ["tailscale", "ssh", username + "@" + hostname, command]
    logging.debug(" ".join(subprocess_command))
    try:
        completed = subprocess.run(
            subprocess_command,
            check=True,
            timeout=15,
            capture_output=True,
        )
    except subprocess.CalledProcessError as err:
        logging.debug("err.cmd: %r", err.cmd)
        logging.debug("err.output: %r", err.output)
        logging.debug("err.stdout: %r", err.stdout)
        logging.debug("err.stderr: %r", err.stderr)
        return False
    except subprocess.TimeoutExpired as err:
        logging.debug("tailscale ssh timeout", exc_info=err)
        return False
    else:
        logging.debug(completed)
        return True

def get_notebook(
    client,
    existing_notebook_policy,
    max_notebooks,
    any_free,
    options,
    wait_interval=EXISTING_NOTEBOOK_WAIT_INTERVAL,
):
    """
    Handle any existing running notebooks according to the policy.

    Options:
        "abort" the ID of the policy (return ID string)
        "force" the notebook to stop
        "wait" for the notebook to stop
        "use" the existing notebook

    Returns the ID of any running notebook (else None)
    """

    # Get ID of any existing running notebook
    while True:
        # Get current notebooks
        notebooks = client.list(tags=())

        # Identify any currently active notebooks
        running = []
        for n in notebooks:
            logging.debug("Notebook %r has state %r", n.id, n.state)
            if n.state in ("Running", "Provisioned"):
                running.append(n)

        if not running:
            logging.debug("No running notebooks")
            break

        assert (
            len(running) == 1
        ), "More than one running notebook, don't know what to do"
        n = running.pop()

        # Just abort
        if existing_notebook_policy == "abort":
            logging.info("There is an existing running notebook with ID %r", n.id)
            return None

        if existing_notebook_policy == "use":
            logging.info("Using existing running notebook with ID %r", n.id)
            return n.id

        # Force existing notebook to stop
        if existing_notebook_policy == "force":
            logging.info("Stopping notebook %r", n.id)
            client.stop(id=n.id)
            break

        # Make sure policy is "wiat", only valid remaining option
        if existing_notebook_policy != "wait":
            raise ValueError(
                "Unrecognized existing notebook policy {!r}".format(
                    existing_notebook_policy
                )
            )

        # Wait for the existing notebook to stop
        logging.info(
            "Waiting %d seconds for running notebook %r to stop...", wait_interval, n.id
        )
        time.sleep(wait_interval)

    return start_new_notebook(client, max_notebooks, any_free, **options)


def start_new_notebook(client, max_notebooks, any_free, **options):
    """
    Delete least recently modified notebooks to make space for new one.
    """
    notebooks = sorted(client.list(tags=()), key=lambda n: n.dt_modified, reverse=True)
    for n in notebooks:
        logging.debug("Notebook %r was last modified %s", n.id, n.dt_modified.ctime())
    while len(notebooks) >= max_notebooks:
        n = notebooks.pop()
        logging.info(
            "Deleting notebook %r (last modified %r)", n.id, n.dt_modified.ctime()
        )
        client.delete(id=n.id)

    if any_free and options["machine_type"].startswith("Free-"):
        # Make list with specified free machine type first
        machine_types = [options["machine_type"]] + [m for m in FREE_MACHINE_TYPES if m != options["machine_type"]]
    else:
        machine_types = [options["machine_type"]]

    for i, machine_type in enumerate(machine_types):
        # Start a new notebook
        logging.info("Creating new notebook with VM machine type %r...", machine_type)
        try:
            revised_options = options | {"machine_type": machine_type}
            ret = client.create(**revised_options)
        except ResourceFetchingError as err:
            #logging.error("Failed to create notebook")#, exc_info=err)
            #logging.error(err)
            #logging.error(err.args[0])
            if err.args and "out of capacity for the selected VM type" in err.args[0]:
                logging.error("Failed to create notebook because Paperspace is out of VM capacity for %r", options["machine_type"])
                if i == len(machine_types) - 1:
                    raise
            else:
                raise
        else:
            return ret

def main(
    verbose,
    max_notebooks,
    existing_notebook_policy,
    ssh_username,
    options_filename,
    any_free,
):
    # Set up logging
    logging.basicConfig(level=logging.DEBUG if verbose else logging.INFO)

    options = load_options(options_filename)
    # WARNING: uncommenting the following risks leaking secrets (set in environment variables)
    # from pprint import pformat
    # logging.debug("options:\n%s", pformat(options))

    # Start a gradient api client
    client = NotebooksClient(api_key=options.pop("apiKey", None))

    # Handle any existing notebooks according to chosen policy
    notebook_id = get_notebook(client, existing_notebook_policy, max_notebooks, any_free, options)

    if notebook_id is None:
        return 1

    print(notebook_id)

    # Wait for the notebook container's hostname to appear among tailscale peers
    logging.info("Waiting for %r to be live on the tailscale network...", notebook_id)
    wait_until_live(hostname=notebook_id)

    # Attempt to establish an SSH connection (via Tailscale VPN)
    logging.info("Attempting SSH connection:")
    if ssh_username is None:
        logging.debug(
            "Setting SSH username from env var %s",
            DEFAULT_SSH_USERNAME_ENVIRONMENT_VARIABLE,
        )
        ssh_username = os.environ[DEFAULT_SSH_USERNAME_ENVIRONMENT_VARIABLE]
    logging.info("Attempting SSH connection to %s@%s", ssh_username, notebook_id)
    if test_ssh_connection(hostname=notebook_id, username=ssh_username):
        logging.info("Connection success!")
        return 0
    logging.error("SSH connection failed")
    return 1


def parse_args(**kwargs):
    """
    Parse command-line arguments.
    """
    parser = ArgumentParser(
        description="Start a Paperspace Gradient notebook accessible via Tailscale SSH"
    )
    parser.add_argument("-v", "--verbose", action="count")
    parser.add_argument(
        "--max-notebooks",
        type=int,
        default=DEFAULT_MAX_NOTEBOOKS,
        help="Maximum allowed number of notebooks (regardless of state)",
    )
    existing_notebook_policy = parser.add_mutually_exclusive_group()
    existing_notebook_policy.add_argument(
        "--force",
        action="store_const",
        dest="existing_notebook_policy",
        const="force",
        help="Stop existing running notebook and start a new one",
    )
    existing_notebook_policy.add_argument(
        "--wait",
        action="store_const",
        dest="existing_notebook_policy",
        const="wait",
        help="Wait for existing running notebook to terminate, then start a new one",
    )
    existing_notebook_policy.add_argument(
        "--abort",
        action="store_const",
        dest="existing_notebook_policy",
        const="abort",
        help="Abort if there is an existing running notebook, leaving it running",
    )
    existing_notebook_policy.add_argument(
        "--use",
        action="store_const",
        dest="existing_notebook_policy",
        const="use",
        help="If there is an existing running notebook, use it instead of starting a new one",
    )
    parser.set_defaults(existing_notebook_policy=DEFAULT_EXISTING_NOTEBOOK_POLICY)
    parser.add_argument(
        "--ssh-username",
        type=str,
        # default=os.environ[DEFAULT_SSH_USERNAME_ENVIRONMENT_VARIABLE],
        help="Username for SSH connection (depends on the notebook's container configuration)",
    )
    parser.add_argument(
        "--options-file",
        type=Path,
        default=DEFAULT_NOTEBOOK_OPTIONS_FILE,
        dest="options_filename",
        help="Options file for Gradient notebook settings, in YAML + Jinja2 format",
    )
    parser.add_argument(
        "--any-free",
        action="store_true",
        help="Try all free Paperspace Gradient machine types if the selected one isn't available",
    )

    return parser.parse_args()


if __name__ == "__main__":
    sys.exit(main(**vars(parse_args())))
