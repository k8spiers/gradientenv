#!/usr/bin/env sh
set -e

die () {
    echo >&2 "$@"
    exit 1
}

NUM_ARGS_REQUIRED=0
[ "$#" -eq ${NUM_ARGS_REQUIRED} ] || die "${NUM_ARGS_REQUIRED} argument(s) required, $# provided"
echo >&2 "Args:\n$@"

curl \
	--silent \
	--location \
	--header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN:?}" \
	--request DELETE \
	-o response.json \
	--data "token=${GITLAB_RUNNER_TOKEN:?}" \
	-w "%{response_code}" \
	"${CI_API_V4_URL:?}/runners"

