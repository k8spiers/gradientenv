# syntax = docker/dockerfile:1.4

ARG MICROMAMBA_TAILSCALE_VERSION=v0.4.0

# Check gitlab-runner versions here: https://gitlab.com/gitlab-org/gitlab-runner/-/releases
# e.g. "v16.6.1"
ARG GITLAB_RUNNER_VERSION=latest

FROM registry.gitlab.com/cusk/micromamba-tailscale:${MICROMAMBA_TAILSCALE_VERSION:?} as micromamba

FROM micromamba as manager
# NB: gradient installation fails under python > 3.9 as of 2023/12/18
RUN micromamba install -qy -n base -c conda-forge python=3.9 && micromamba clean -qya
RUN micromamba run -n base pip install --no-cache --disable-pip-version-check gradient

# Base image which takes a conda/mamba/micromamba "conda-lock.yml", APT_PACKAGES and a GITLAB_RUNNER_VERSION as arguments
FROM micromamba as base

USER root

# Add s6-overlay files to run gitlab-runner
COPY s6-rc.d /etc/s6-overlay/s6-rc.d

# Install minio client (mc)
# Check available MC_VERSION here: https://hub.docker.com/r/minio/mc/tags
# MC_VERSION can be set to release date/time e.g. "2023-11-10T21-37-17Z" or blank for latest (default)
#ONBUILD ARG MC_VERSION=""
#ONBUILD RUN \
#curl -sL -o /usr/local/bin/mc https://dl.min.io/client/mc/release/linux-amd64/archive/mc${MC_VERSION:+.RELEASE.${MC_VERSION}} && \
#chmod +x /usr/local/bin/mc

# Enable CUDA support
# As per https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/user-guide.html#dockerfiles
ENV NVIDIA_DRIVER_CAPABILITIES=all
ENV NVIDIA_VISIBLE_DEVICES=all
# ENV NVIDIA_REQUIRE_CUDA="cuda>=11.1" # doesn't seem necessary

# Install gitlab-runner, git, and APT_PACKAGES
ONBUILD ARG GITLAB_RUNNER_VERSION
ONBUILD ARG APT_PACKAGES
ONBUILD RUN \
--mount=type=cache,target=/var/cache/apt,sharing=locked \
--mount=type=cache,target=/var/lib/apt,sharing=locked \
if [ -n "${APT_PACKAGES}${GITLAB_RUNNER_VERSION}" ]; then \
  APT_PACKAGES="${APT_PACKAGES}${GITLAB_RUNNER_VERSION:+ git}" && \
  apt-get update && \
  apt-get --no-install-recommends install -y ${APT_PACKAGES}; \
fi

ONBUILD RUN \
if [ -n "${GITLAB_RUNNER_VERSION}" ]; then \
  curl -sLJ -o /usr/local/bin/gitlab-runner \
    "https://gitlab-runner-downloads.s3.amazonaws.com/${GITLAB_RUNNER_VERSION}/binaries/gitlab-runner-linux-amd64" && \
  ls -l /usr/local/bin/gitlab-runner && \
  chmod +x /usr/local/bin/gitlab-runner; \
fi

ONBUILD USER ${MAMBA_USER}

# Install conda environment  (must include 'jupyterlab')
# grep command is to make sure there is an uncommented line with '- name: jupterlab' in conda-lock.yml
ONBUILD RUN --mount=type=bind,source=conda-lock.yml,target=conda-lock.yml \
grep -qE '^- name: jupyterlab' <(grep -Ev '^\s*#.*' < conda-lock.yml) && \
micromamba install -y -n base -f conda-lock.yml && \
micromamba clean -qya

FROM base as default

