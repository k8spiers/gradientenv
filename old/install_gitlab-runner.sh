#!/usr/bin/env bash
set -e -o pipefail

apt-get update
apt-get install -y git
GITLAB_RUNNER_VERSION=${GITLAB_RUNNER_VERSION:-v15.9.1}
curl -sLJO "https://gitlab-runner-downloads.s3.amazonaws.com/${GITLAB_RUNNER_VERSION}/deb/gitlab-runner_amd64.deb"
dpkg -i gitlab-runner_amd64.deb
rm gitlab-runner_amd64.deb
