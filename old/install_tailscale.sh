#!/usr/bin/env bash
set -e -o pipefail

# Make sure we're in a docker environment
[[ ! -e /.dockerenv ]] && (
	echo >&2 "Not on docker"
	exit 1
)

mkdir -p /usr/share/keyrings
chmod 0755 /usr/share/keyrings
apt-get update
apt-get install -y curl ca-certificates openssh-client
curl -fsSL https://pkgs.tailscale.com/stable/ubuntu/jammy.noarmor.gpg >/usr/share/keyrings/tailscale-archive-keyring.gpg
curl -fsSL https://pkgs.tailscale.com/stable/ubuntu/jammy.tailscale-keyring.list >/etc/apt/sources.list.d/tailscale.list
apt-get update
apt-get install -y tailscale
