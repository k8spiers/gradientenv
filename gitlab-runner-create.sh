#!/usr/bin/env sh
set -e

die () {
    echo >&2 "$@"
    exit 1
}

NUM_ARGS_REQUIRED=0
[ "$#" -eq ${NUM_ARGS_REQUIRED} ] || die "${NUM_ARGS_REQUIRED} argument(s) required, $# provided"
echo >&2 "Args:\n$@"

curl \
	--silent \
	--location \
	--header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN:?}" \
	--request POST \
	-o response.json \
	-F "runner_type=project_type" \
	-F "project_id=${CI_PROJECT_ID:?}" \
	-F "run_untagged=false" \
	-F "locked=true" \
	-F "tag_list=git-${CI_COMMIT_SHORT_SHA:?}" \
	-F "maximum_timeout=3500" \
	-w "%{response_code}" \
	"${CI_API_V4_URL:?}/user/runners"

